package piglatintranslator;

import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class TranslatorParameterizedTest {

	private Translator translator;
	private String input;
	private String expectedOutput;
	
	public TranslatorParameterizedTest(String input,String expectedOutput) {
		super();
		this.input=input;
		this.expectedOutput=expectedOutput;
	}
	
	@Before
	public void initialize() {
		translator = new Translator();
	}
	
	@Parameterized.Parameters(name = "Test {index}: Original phrase: {0} \t Translated phrase: {1}")
	public static Collection<Object[]> input() {
		return Arrays.asList(new Object[][] {
			{"", Translator.NIL},
			{"any","anynay"},
			{"utility","utilitynay"},
			{"apple","appleyay"},
			{"ask","askay"},
			{"hello","ellohay"},
			{"known","ownknay"},
			{"hello world","ellohay orldway"},
			{"well-being","ellway-eingbay"},
			{"hello world!","ellohay orldway!"},
			{"APPLE","APPLEAY"},
		});
	}
	
	@Test
	public void testTranslatorTest() {
		translator.setPhrase(input);
		assertEquals(expectedOutput, translator.translate());
	}
	

}
