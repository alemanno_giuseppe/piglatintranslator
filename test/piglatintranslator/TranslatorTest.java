package piglatintranslator;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TranslatorTest {

	private Translator translator;

	@Test
	public void testInputPhrase() {
		String inputPhrase = "hello world";
		translator = new Translator(inputPhrase);
		assertEquals("hello world", translator.getPhrase());	
	}

	@Test
	public void testTranslationEmptyPhrase() {
		String inputPhrase = "";
		translator = new Translator(inputPhrase);
		assertEquals(Translator.NIL, translator.translate());	
	}

	@Test
	public void testTranslationWordStartingWithAEndingWithY() {
		String inputPhrase = "any";
		translator = new Translator(inputPhrase);
		assertEquals("anynay", translator.translate());	
	}

	@Test
	public void testTranslationWordStartingWithUEndingWithY() {
		String inputPhrase = "utility";
		translator = new Translator(inputPhrase);
		assertEquals("utilitynay", translator.translate());	
	}

	@Test
	public void testTranslationWordStartingWithVowelEndingWithVowel() {
		String inputPhrase = "apple";
		translator = new Translator(inputPhrase);
		assertEquals("appleyay", translator.translate());	
	}

	@Test
	public void testTranslationWordStartingWithVowelEndingWithConsonant() {
		String inputPhrase = "ask";
		translator = new Translator(inputPhrase);
		assertEquals("askay", translator.translate());	
	}

	@Test
	public void testTranslationWordStartingWithSingleConsonant() {
		String inputPhrase = "hello";
		translator = new Translator(inputPhrase);
		assertEquals("ellohay", translator.translate());	
	}

	@Test
	public void testTranslationWordStartingWithMoreConsonant() {
		String inputPhrase = "known";
		translator = new Translator(inputPhrase);
		assertEquals("ownknay", translator.translate());	
	}

	@Test
	public void testTranslationPhraseWithNotCompositeWords() {
		String inputPhrase = "hello world";
		translator = new Translator(inputPhrase);
		assertEquals("ellohay orldway", translator.translate());	
	}

	@Test
	public void testTranslationPhraseWithCompositeWords() {
		String inputPhrase = "well-being";
		translator = new Translator(inputPhrase);
		translator.translate();
		assertEquals("ellway-eingbay", translator.translate());	
	}
	
	@Test
	public void testTranslationPhraseContainingPunctuations() {
		String inputPhrase = "hello world!";
		translator = new Translator(inputPhrase);
		translator.translate();
		assertEquals("ellohay orldway!", translator.translate());	
	}
	
	@Test
	public void testTranslationPhraseContainingTitleCaseWords() {
		String inputPhrase = "APPLE";
		translator = new Translator(inputPhrase);
		translator.translate();
		assertEquals("APPLEAY", translator.translate());	
	}
}
