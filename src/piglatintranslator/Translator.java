package piglatintranslator;

import jdk.nashorn.internal.ir.ReturnNode;

public class Translator {
	public static final String NIL = "nil";

	private String phrase; 
	
	public Translator(String inputPhrase) {
		phrase = inputPhrase;
	}

	public Translator() {
		// TODO Auto-generated constructor stub
	}

	public String getPhrase() {
		return phrase;
	}

	public String translate() {
		String[] words = phrase.split("(?= )|(?=-)|(?=\\()|(?=\\))|(?=!)|(?=;)|(?=:)|(?=\\?)|(?=\\.)|(?=')");
		for (int i = 0; i < words.length; i++) {
			words[i] = translateSingleWord(words[i]);
		} 
		if (!"".equals(phrase)) {
			return compactsTranslatedWords(words);
		}
		return NIL;
	}

	private String compactsTranslatedWords(String[] words) {
		StringBuilder translatedPhrase = new StringBuilder();
		translatedPhrase.append(words[0]);

		for (int i = 1; i < words.length; i++) {
				translatedPhrase.append(words[i]);
		}
		return translatedPhrase.toString();
	}

	private String translateSingleWord(String word) {
		String punctuation ="";
		String translation ="";
		if(startWithPunctuation(word)) {
			punctuation  = word.substring(0,1);
			word = word.substring(1);
		}
		if (startWithVowel(word)){
			translation = translateWordStartingWithVowel(word, punctuation);
		} else if ("".equals(word)) {
			return punctuation;
		} else {
			translation = translateWordStartingWithConsonant(word, punctuation);
		}
		if(wordIsTitleCase(word))
			return translation.toUpperCase();
		return translation;
	}

	private boolean wordIsTitleCase(String word) {
		for (int i = 0; i < word.length(); i++) {
			if (!Character.isUpperCase(word.charAt(i))) {
				return false;
			}
		}
		return true;
	}

	private String translateWordStartingWithConsonant(String word, String punctuation) {
		String firstConsecutiveConsonants = word.substring(positionOfLastConsecutiveConsonantAtTheBeginningOfPhrase());
		return punctuation + firstConsecutiveConsonants + word.substring(0,positionOfLastConsecutiveConsonantAtTheBeginningOfPhrase()) + "ay";
	}

	private String translateWordStartingWithVowel(String word, String punctuation) {
		if (word.endsWith("y")) {
			return punctuation + word + "nay";
		} else if (endWithVowel(word)) {
			return punctuation + word + "yay";
		} 
		return punctuation + word + "ay";
	}
	
	private int positionOfLastConsecutiveConsonantAtTheBeginningOfPhrase() {
		for (int i=0;i<phrase.length(); i++) {
			char character = phrase.charAt(i);
			if (character=='a'||character=='e'||character=='i'||character=='o'||character=='u') {
				return i;
			}
		}
		return phrase.length();
	}
	
	private boolean startWithVowel(String word) {
		return word.startsWith("a")||word.startsWith("e")||
			   word.startsWith("i")||word.startsWith("o")||word.startsWith("u");
	}
	
	private boolean endWithVowel(String word) {
		return word.endsWith("a")||word.endsWith("e")||
			   word.endsWith("i")||word.endsWith("o")||word.endsWith("u");
	}

	private boolean startWithPunctuation(String word) {
		return word.startsWith("-")||word.startsWith(" ")||word.startsWith("(")||
			   word.startsWith(")")||word.startsWith("!")||word.startsWith("?")||
			   word.startsWith(":")||word.startsWith(".")||word.startsWith("'");
	}

	public void setPhrase(String input) {
		this.phrase = input;
		
	}


}
